package cp437tree

const (
	VerticalRight = "├"
	DownLeft      = "┐"
	UpRight       = "└"
	DownRight     = "┌"
	Horizontal    = "─"
	Vertical      = "│"
	Solo = "╶"
)

type Tree struct {
	Text string
	Children []Tree
}

type TreeMeta struct {
	Tree Tree
	Depth int
}

func (t *Tree) String() string {
	return t.Text
}

func (t *Tree)Walk() *TreeMeta {
	return nil
}

/*
┌┐ // HeaderTop // if not only item, otherwise just Solo
├┐ // HeaderMid //check depth
│├ // ItemMid // check depth
│└ // ItemBottom //check depth

└┐ // HeaderBottom // HARD
 └── //ItemEndBottom // HARD
*/

//
//func (o *Request) ToTreeGroup(w io.Writer, position rune) {
//	middle := "├──┐" // default middle branch
//	midRow := "│  ├── "
//	botRow := "│  └── "
//	topBranch := "┌──┐"
//	endBranch := "└──┐"
//	veryBottomRow := "   └── "
//
//	rowChars := ""
//	if position == 't' {
//		branch = topBranch
//	}
//	if position == 'b' {
//		branch = endBranch
//	}
//
//	// platform name header
//	rowChars = midRow
//	// preparing the children variable so this whole thing can be extracted later.
//	children := o.Versions
//	childCount := len(children) - 1
//	for idx, v := range children {
//		if idx == childCount {
//			rowChars = botRow
//			if position == 'b' {
//				rowChars = veryBottomRow
//			}
//		}
//		_, _ = fmt.Fprintf(w, "\n%s%s", rowChars, v)
//	}
//
//	// new line to end the row
//	_, _ = fmt.Fprintf(w, "\n")
//}
//
//func idToPosition(id, count int) rune {
//	// Returns 't' for top, 'b' for bottom, and 'm', for middle by comparing the idx to the count of rows.
//	// Does not account for if count = 1
//	if id == 0 {
//		return 't'
//	}
//	if id == count-1 {
//		return 'b'
//	}
//	return 'm'
//}
//
func main() {

}
